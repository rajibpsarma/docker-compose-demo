# docker-compose-demo #

It's a demo example showing the use of docker compose to start / stop multiple related docker containers.

### It uses the following docker examples : ###
* [spring_boot_uploader_job](https://bitbucket.org/rajibpsarma/spring_boot_uploader_job)
* [spring_boot_uploader_cand](https://bitbucket.org/rajibpsarma/spring_boot_uploader_cand)
* [uploader-ui](https://bitbucket.org/rajibpsarma/uploader-ui)

### Steps: ###
* Get the related projects as shown above.
* Get docker-compose.yml file. The directory structure should be as shown below:

    docker-compose-demo			
    |-- docker-compose.yml	
    |-- spring_boot_uploader_cand	
    |           		  |-- Dockerfile	
    |        	 	+  ...	
    |-- spring_boot_uploader_job	
    |           	  	|-- Dockerfile	
    |         		    +  ...	
    |-- spring_boot_uploader_libs	
    |         		    +  ...	
    +-- spring_boot_uploader_UI			
		    		    |-- Dockerfile			
		    		    +  ...	
						

* Create all the 3 containers and start them all together, using command "sudo docker-compose up -d".
* Check that the containers are up, using command "sudo docker ps -a".
* Open the UI page in the browser using "http://localhost:8083/"
* Stop and remove the containers using "sudo docker-compose down".


